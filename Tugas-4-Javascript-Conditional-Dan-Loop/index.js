//soal 1
var nilai = 90;

if(nilai >= 85 ){
    console.log('indeksnya A');
}else if(nilai >= 75 && nilai < 85){
    console.log('indeksnya B');
}else if(nilai >= 65 && nilai <75){
    console.log('indeksnya C');
}else if(nilai >= 55 && nilai <65){
    console.log('indeksnya D');
}else{
    console.log('indeksnya E');
}


//soal 2
var tanggal = 22;
var bulan = 7;
var tahun = 2020;

switch(bulan){
    case 1 : {
        bulan = 'Januari';
        break;
    }
    case 2 : {
        bulan = 'Februari';
        break;
    }
    case 3 : {
        bulan = 'Maret';
        break;
    }
    case 4 : {
        bulan = 'April';
        break;
    }
    case 5 : {
        bulan = 'Mei';
        break;
    }
    case 6 : {
        bulan = 'Juni';
        break;
    }
    case 7 : {
        bulan = 'Juli';
        break;
    }
    case 8 : {
        bulan = 'Agustus';
        break;
    }
    case 9 : {
        bulan = 'September';
        break;
    }
    case 10 : {
        bulan = 'Oktober';
        break;
    }
    case 11 : {
        bulan = 'November';
        break;
    }
    case 12 : {
        bulan = 'Desember';
        break;
    }
}
console.log(tanggal + " " + bulan + " " + tahun);


//soal 3
//n=3
var x = "";
for(var n = 0; n < 3; n++){
    for(var i = 0; i <= n; i++){
        x += "#";
    }
    x += "\n";
}
console.log(x);

//n=7
var x = "";
for(var n = 0; n < 7; n++){
    for(var i = 0; i <= n; i++){
        x += "#";
    }
    x += "\n";
}
console.log(x);

//soal 4
var m=10;
var zzz = "==="
var kal = "";

for(var i = 1; i <= m; i++){
    switch(i % 3){
        case 1 : { kal += i + "-" + "I love Programming" + "\n"; break;}
        case 2 : { kal += i + "-" +  "I love Javascript" + "\n"; break;}
        case 0 : { kal += i + "-" +  "I love VueJs" + "\n" + zzz + "\n"; zzz += "==="; break;}
    }
}
console.log(kal);