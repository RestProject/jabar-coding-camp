//soal 1
var pertama = 'saya senang belajar hari ini';
var kedua = 'belajar javascript itu keren';

var kalPertama = pertama.substring(0,11);
var kalKedua = kedua.substring(0,18);
console.log(kalPertama.concat(" ", kalKedua));

//soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

var kataPertama = parseInt(kataPertama);
var kataKedua = parseInt(kataKedua);
var kataKetiga = parseInt(kataKetiga);
var kataKeempat = parseInt(kataKeempat);

console.log((kataPertama+kataKedua)*(kataKeempat-kataKetiga));

//soal 3
var kalimat = 'wah javascript itu keren sekali'; 

var katPertama = kalimat.substring(0, 3); 
var katKedua = kalimat.substring(4,14);
var katKetiga = kalimat.substring(15,18); 
var katKeempat = kalimat.substring(19,24);
var katKelima = kalimat.substring(25);

console.log('Kata Pertama: ' + katPertama); 
console.log('Kata Kedua: ' + katKedua); 
console.log('Kata Ketiga: ' + katKetiga); 
console.log('Kata Keempat: ' + katKeempat); 
console.log('Kata Kelima: ' + katKelima);