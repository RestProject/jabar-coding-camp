//soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort();

daftarHewan.forEach(function(hewan){
    console.log(hewan);
});


//soal 2
function introduce(data){
    return "Nama Saya " + data.name + ", umur saya " + data.age + " tahun, alamat saya di " + data.address + ", dan saya punya hobby yaitu " + data.hobby ; 
}
    
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data)
console.log(perkenalan);


//soal 3
function hitungHurufVokal(data){
    var vokal = "aiueo";
    var dummy;
    var c = 0;
    dataBaru = data.split('');
    dataBaru.forEach(huruf => {
        dummy = vokal.search(huruf);
        if(dummy !== -1){
            c++;
        } 
    });
    return c ;
}

var hitung_1 = hitungHurufVokal("Rendi Restiandi");
console.log(hitung_1);


//soal 4
function hitung(data){
    var awal = -2;
    for(var i = 0; i<data; i++){
        awal += 2;
    }
    return awal;
}

console.log(hitung(5));